from OpenGL.GL import *
from OpenGL.GLUT import *
from OpenGL.GLU import *

ESCAPE = '\033'

class Cube:
    #def __windowLength

    def __init__(self, windowHeight, windowWidth):
        self.__windowHeight = windowHeight
        self.__windowWidth = windowWidth

    def InitGL(self):
        glClearColor(0.0, 0.0, 0.0, 0.0)
        glClearDepth(1.0)
        glDepthFunc(GL_LESS)
        glEnable(GL_DEPTH_TEST)
        glShadeModel(GL_SMOOTH)
        glutKeyboardFunc(self.__keyPressed)

    def DrawGLScene(self):
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        glLoadIdentity()
        glTranslatef(0.0, 0.0, -4.5)
        self.__drawCube()
        glutSwapBuffers()

    def __keyPressed(self, *args):
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()

        if args[0] == "1":
            gluPerspective(45.0, float(self.__windowHeight) / float(self.__windowWidth), 0.1, 100.0)
        if args[0] == "2":
            glOrtho(-5.0, 5.0, -5.0, 5.0, -4.0, 9.0)
        if args[0] == "3":
            gluPerspective(60.0, 1.5, 1.0, 10.0)
        if args[0] == "4":
            gluPerspective(45.0, 1.5, 1.0, 15.0)
        if args[0] == "5":
            gluPerspective(60.0, 1.5, 5.0, 20.0)
        glMatrixMode(GL_MODELVIEW)


    def Draw(self):
        global window

        glutInit(sys.argv)
        glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH)
        glutInitWindowSize(self.__windowHeight, self.__windowWidth)
        glutInitWindowPosition(200, 200)

        window = glutCreateWindow('OpenGL Python Cube')

        glutDisplayFunc(self.DrawGLScene)
        glutIdleFunc(self.DrawGLScene)
        self.InitGL()
        glutMainLoop()

    def __drawCube(self):
        glBegin(GL_QUADS)

        glColor3f(252.0, 0.0, 127.0)
        glVertex3f(1.0, 1.0, -1.0)
        glVertex3f(-1.0, 1.0, -1.0)
        glVertex3f(-1.0, 1.0, 1.0)
        glVertex3f(1.0, 1.0, 1.0)

        glColor3f(252.0, 0.0, 127.0)
        glVertex3f(1.0, -1.0, 1.0)
        glVertex3f(-1.0, -1.0, 1.0)
        glVertex3f(-1.0, -1.0, -1.0)
        glVertex3f(1.0, -1.0, -1.0)

        glColor3f(0.0, 1.0, 0.0)
        glVertex3f(1.0, -1.0, -1.0)
        glVertex3f(-1.0, -1.0, -1.0)
        glVertex3f(-1.0, 1.0, -1.0)
        glVertex3f(1.0, 1.0, -1.0)

        glColor3f(204.0, 0.0, 0.0)
        glVertex3f(-1.0, 1.0, 1.0)
        glVertex3f(-1.0, 1.0, -1.0)
        glVertex3f(-1.0, -1.0, -1.0)
        glVertex3f(-1.0, -1.0, 1.0)

        glColor3f(204.0, 0.0, 0.0)
        glVertex3f(1.0, 1.0, -1.0)
        glVertex3f(1.0, 1.0, 1.0)
        glVertex3f(1.0, -1.0, 1.0)
        glVertex3f(1.0, -1.0, -1.0)

        glEnd()